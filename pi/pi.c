#include <stdio.h>
#include <float.h>
#include "core.h"


/**
 * 
 * Arguments: 1 = steps
 */
int main(int argc, char **argv)
{
    int Digs = DECIMAL_DIG;
    int steps = 10000000;
    printf("%.*e\n",Digs, integrate(circle, -1.0, 1.0, steps));
    return 0;
}

