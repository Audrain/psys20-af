#ifndef _CORE_H
#define _CORE_H

double circle(double x);
double integrate(double (*f)(double x), double l, double r, int steps);


#endif
