/**
 * @file pi.c
 *
 * berechnen PI
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

#include "core.h"

double circle(double x)
{
    return 2.0 * sqrt(1.0 - x * x);
}

double integrate(double (*f)(double x), double l, double r, int steps)
{
    double sum = 0.0;
    int start = (int)(l * steps);
    int end = (int)(r * steps);
	int i;

    
    #pragma omp parallel for firstprivate(start, end) reduction(+: sum)
    for (i = start; i < end; i++)
    {
        sum += f((double) i / steps) * 1.0 / steps;
    }

    return sum;
}
