/**
 * Test for functions in core.c
 *
 * We don't need to test anything here really.
 */
#define _USE_MATH_DEFINES

#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <omp.h>
/* Include the module under testing */
#include "core.c"

/******************************************************/

static void test_pi_calculation(void)
{
 assert ((trunc(M_PI*10000)/10000) == trunc(integrate(circle, -1.0, 1.0, 1000000)*10000)/10000);

}

int main(int argc, char **argv)
{
    test_pi_calculation();
    return 0;
}

