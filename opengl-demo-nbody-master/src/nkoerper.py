#!/usr/bin/env python3

from typing import List, Optional, Dict, Tuple
from urllib.parse import quote
from urllib.request import urlopen
import re
import json


base_url = "https://ssd.jpl.nasa.gov/horizons_batch.cgi?batch=1"


def convert_table(table_lines: List[str]) -> List[Dict[str, str]]:
    seperators = list()
    curr_start = -1

    headers = table_lines[0].split()

    for i, char in enumerate(table_lines[1]):
        if char == "-":
            if curr_start == -1:
                curr_start = i
        else:
            if curr_start >= 0:
                seperators.append([curr_start, i])
                curr_start = -1

    fields = list()
    for line in table_lines[2:]:
        raw_fields = [line[sep[0]:sep[1]].strip() for sep in seperators]
        fields.append({headers[i]: f for i, f in enumerate(raw_fields)})

    return fields


def cut_split_and_format(in_str: str, start_split: int, end_split: int) -> List[str]:
    lines = in_str.splitlines()
    lines = lines[start_split:-end_split]
    lines = [l[2:] for l in lines]
    return lines


def search_planet(planet: str, table_fields: List[Dict[str, str]]) -> Optional[str]:
    for row in table_fields:
        if planet == row["Name"]:
            return row["ID#"]

    return None


def build_url(**kwargs) -> str:
    new_url = base_url
    for key, value in kwargs.items():
        if value is None:
            value = "NONE"
        if isinstance(value, bool):
            value = "YES" if value else "NO"
        val_str = quote("'{}'".format(value))
        new_url += "&{}={}".format(key.upper(), val_str)
    return new_url


def request_mayor_bodies() -> str:
    url = build_url(command="mb")

    with urlopen(url) as response:
        response_text = response.read().decode("utf-8")
        return response_text


def request_data(body_id: str) -> str:
    url = build_url(
        command=body_id,
        csv_format=True,
        obj_data=True,
        make_ephem=True,
        table_type="vectors",
        center="@sun",
        ref_plane="frame",
        ref_system="j2000",
        out_units="au-d",
        vec_table=2,
        vec_corr=None,
        start_time="2020-07-29",
        stop_time="2020-07-29",
        step_size="1 d")

    with urlopen(url) as response:
        response_text = response.read().decode("utf-8")
        return response_text


def extract_pos_and_vel(response_lines: List[str]) -> Tuple[Tuple, Tuple]:
    soe = -1
    eoe = -1
    for i, curr_line in enumerate(response_lines):
        if curr_line == "$$SOE":
            soe = i
        elif curr_line == "$$EOE":
            eoe = i

        if soe >= 0 and eoe >= 0:
            break

    csv_line = response_lines[soe + 1: eoe][-1]
    csv_fields = csv_line.split(",")
    position = csv_fields[2:5]
    velocity = csv_fields[5:8]

    position = [float(p.strip()) for p in position]
    velocity = [float(v.strip()) for v in velocity]

    return tuple(position), tuple(velocity)


def find_space_block(string: str, start: int) -> Tuple[int, int]:
    group_start = -1
    for i, char in enumerate(string[start:]):
        if char == " ":
            if group_start < 0:
                group_start = i + start
        elif group_start >= 0:
            maybe_end = i + start
            if maybe_end - group_start == 1:
                group_start = -1
            else:
                return group_start, maybe_end

    if group_start >= 0:
        return group_start, len(string)

    return -1, -1


def extract_property_dict(response_lines: List[str], searched_field_starts: List[str] = None) -> Dict:
    assert searched_field_starts

    start_line = -1
    end_line = -1

    for i, line in enumerate(response_lines):
        if line and line.count("*") == len(line):
            if start_line == -1:
                start_line = i
            elif end_line == -1:
                end_line = i

        if start_line >= 0 and end_line >= 0:
            break

    property_lines = response_lines[start_line + 1:end_line]

    assert property_lines

    props = dict()

    def _extract_values(line):
        name, value = line.split("=")

        name = name.strip()
        value = value.strip()
        for start in searched_field_starts:
            if name.strip().startswith(start):
                props[name] = value
                return

    double_value_lines = list()
    for line in property_lines:
        num_fields = line.count("=")

        if num_fields < 1:
            continue

        if num_fields == 2:
            double_value_lines.append(line)
        elif num_fields == 1:
            _extract_values(line)
        else:
            print("Encountered a stupid number of equals inside a row of a "
                  "property table. Just ignoring it. Maybe NASA should hire "
                  "people who can code sane API's")

    split_pos = -1

    for line in double_value_lines:
        equal_pos = line.find("=")

        blk_start, blk_end = find_space_block(line, equal_pos)

        # if start of the space block is directly after the equal char ignore
        # it and try after this block again
        if blk_start == equal_pos + 1:
            blk_start, blk_end = find_space_block(line, blk_end)

        if line[blk_end] == "=":
            continue

        if blk_start >= 0:
            split_pos = blk_end
            break

    assert split_pos > 0

    for line in double_value_lines:
        first = line[:split_pos]
        second = line[split_pos:]

        _extract_values(first)
        _extract_values(second)

    return props


def parse_planet_response(planet_response_str: str):
    lines = planet_response_str.split("\n")

    props = extract_property_dict(lines, ["Mass"])
    position, velocity = extract_pos_and_vel(lines)

    planet_data = dict()
    for key, value in props.items():
        k_match = re.search("Mass,?\s\(?x?(10\^[0-9]+)\s\(?kg\s?\)?", key)

        if k_match:
            exponent = int(k_match.group(1).split("^")[1])

            e_match = re.search("\(?10\^([\+\-]?[0-9]+)\)?", value)
            if e_match:
                value_exponent = int(e_match.group(1))
                exponent += value_exponent

            v_match = re.search("([0-9]+\.?[0-9]+)", value)
            assert v_match
            value = float(v_match.group(1))

            planet_data["mass"] = float("{}E{}".format(value, exponent))

            break

    planet_data["position"] = position
    planet_data["velocity"] = velocity

    return planet_data


requested_planets = [
    "Sun",
    "Mercury",
    "Venus",
    "Earth",
    "Moon",
    "Mars",
    "Phobos",
    "Deimos",
    "Jupiter",
    "Io",
    "Europa",
    "Ganymede",
    "Callisto",
    "Saturn",
    "Mimas",
    "Enceladus",
    "Tethys",
    "Dione",
    "Rhea",
    "Titan",
    "Hyperion",
    "Iapetus",
    "Uranus",
    "Miranda",
    "Ariel",
    "Umbriel",
    "Titania",
    "Oberon",
    "Neptune",
    "Triton",
    "Pluto",
]

mayor_bodies = request_mayor_bodies()
table_lines = cut_split_and_format(mayor_bodies, 3, 6)
table_fields = convert_table(table_lines)

requested_ids = list()
for planet in requested_planets:
    id = search_planet(planet, table_fields)
    assert id is not None
    requested_ids.append(id)

planet_data = list()
for planet, id in zip(requested_planets, requested_ids):
    planet_data.append(parse_planet_response(request_data(id)))
    planet_data[-1]["name"] = planet

out_data = dict()
out_data["velocity_unit"] = "au/d"
out_data["position_unit"] = "au"
out_data["mass_unit"] = "kg"
out_data["bodies"] = planet_data

with open("nkoerper.json", "w") as f:
    json.dump(out_data, f, indent=4)
