/*
 * NBody.cpp
 *
 *  Created on: Aug 10, 2012
 *      Author: Antoine Grondin
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <omp.h>
#include "NBody.h"
#include "VectorMath.h"
#include <string>

using namespace std;




using std::cout;
using std::cerr;
using std::endl;

///////////////////////////////////////////////////////////////////////
// Setup

int main( int argc, char **argv ) {
  // printGreetings();
  string fileName = "Nkoerper.json";
string content = readFile(fileName);
cout<<content << "\n\n";
   if( TEST ) {
      if( !testVectorMath() ) {
         cerr << "Test VectorMath failed.";
         return -2;
      }

      cout << "All test OK" << endl;
   }

   setupWindow( argc, argv );
   registerCallbacks();
   // Initialize GLEW
   GLenum anError = glewInit();

   if( anError != 0 ) {
      fprintf( stderr, "GLEW Error: %s\n",
               glewGetErrorString( anError ) );

      if( D ) {
         cerr << " done" << endl;
      }

      return 1;
   }

   setupRenderContext();
   glutMainLoop();
   return 0;
}

void setupWindow( int argc, char **argv ) {
   gltSetWorkingDirectory( argv[0] );
   glutInit( &argc, argv );
   glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
   glutInitWindowSize( APP_WIDTH, APP_HEIGHT );
   glutCreateWindow( "Body SImulation" );
  // glutMouseFunc( onMouseEvent );
   //glutIgnoreKeyRepeat(true);
   glutFullScreen();
}

void registerCallbacks() {
   glutReshapeFunc( onChangeSize );
   glutDisplayFunc( onRenderScene );
}

void setupRenderContext() {
   sShaderManager.InitializeStockShaders();
   glEnable( GL_DEPTH_TEST );
   setupBodies();
   glClearColor( sBackgroundColor[0],
                 sBackgroundColor[1],
                 sBackgroundColor[2],
                 sBackgroundColor[3] );
   glEnable( GL_LINE_SMOOTH );
}

void setupBodies() {
   printf("Setup bodies\n");
      for(int i = 0; i<BODY_COUNT; i++)
    {
        Particle part = Particle();
        part.id = i;
        part.velocity = sBodyVelocity[i];
        part.acceleration = sBodyAcceleration[i];
        part.position = sBodyPosition[i];
        /*
        part.position.x = 1e18*exp(-1.8)*(0.5 - rand());
        part.position.y = 1e18*exp(-1.8)*(.5 - rand());
        part.position.z = 1e18*exp(-1.8)*(.5 - rand());
        */
        part.mass = 2e1f;// 1.98892e30*rand()*10 + 1e20;
        /*
        part.sBodyFrame = GLFrame();
        part.sBodyBatch = GLTriangleBatch();
        */
         part.print();
        printf("Setup bodies\n");

        gltMakeSphere( part.sBodyBatch, part.sBodyRadius, 30, 50 );
        part.sBodyFrame.SetOrigin(part.position.x, part.position.y, part.position.z);
        Particles.push_back(part);
    }
   
  
}

///////////////////////////////////////////////////////////////////////
// Callbacks

void onChangeSize( int aNewWidth, int aNewHeight ) {
   glViewport( 0, 0, aNewWidth, aNewHeight );
   sViewFrustrum.SetPerspective( APP_CAMERA_FOV,
                                 float( aNewWidth ) / float( aNewHeight ),
                                 APP_CAMERA_NEAR,
                                 APP_CAMERA_FAR );
   sProjectionMatrixStack.LoadMatrix(
      sViewFrustrum.GetProjectionMatrix() );
   sTransformPipeline.SetMatrixStacks( sModelViewMatrixStack,
                                       sProjectionMatrixStack );
}

void onRenderScene( void ) {
   // Clear the buffer
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   static CStopWatch timeKeeper;
   // Place camera
   M3DMatrix44f mCamera;
   sCameraFrame.GetCameraMatrix( mCamera );
   sModelViewMatrixStack.PushMatrix( mCamera );
   // Transform the light position into eye coordinates
   M3DVector4f lightPos = { sMainLightPos[0],
                            sMainLightPos[1],
                            sMainLightPos[2],
                            sMainLightPos[3]
                          };
   M3DVector4f lightEyePos;
   m3dTransformVector4( lightEyePos, lightPos, mCamera );
   // Call the drawing functions
   drawBodies( &timeKeeper, &lightEyePos );
   // Switch the buffers to bring the drawing on screen
   glutSwapBuffers();
   glutPostRedisplay();
}

///////////////////////////////////////////////////////////////////////
// Drawing

void drawBodies( CStopWatch *timeKeeper, M3DVector4f *lightPosition ) {
   // compute displacement and new vectors
   clock_t start_time = clock ();
   static float previousTime = 0.0f;
   float currentTime = timeKeeper->GetElapsedSeconds();
   updatePhysics( currentTime - previousTime );
   previousTime = currentTime;
   for(Particle part : Particles)
   {
      sModelViewMatrixStack.PushMatrix();
      // update position with regard to new values
      part.sBodyFrame.SetOrigin(part.position.x, part.position.y, part.position.z);
       // draw
      sModelViewMatrixStack.MultMatrix( part.sBodyFrame );
      sShaderManager.UseStockShader( GLT_SHADER_POINT_LIGHT_DIFF,
                                     sTransformPipeline.GetModelViewMatrix(),
                                     sTransformPipeline.GetProjectionMatrix(),
                                     lightPosition,
                                     part.sBodyColor );
      part.sBodyBatch.Draw();
      // Restore
      sModelViewMatrixStack.PopMatrix();
   }


//printf("Time Take to draw: %f min\n", float( clock () - start_time ) );
   /*
   for( int i = 0; i < BODY_COUNT; i++ ) {

      // Save
      sModelViewMatrixStack.PushMatrix();
      // update position with regard to new values
      sBodyFrames[i].SetOrigin( sBodyPosition[i].x,
                                sBodyPosition[i].y,
                                sBodyPosition[i].z );
      // draw
      sModelViewMatrixStack.MultMatrix( sBodyFrames[i] );
      sShaderManager.UseStockShader( GLT_SHADER_POINT_LIGHT_DIFF,
                                     sTransformPipeline.GetModelViewMatrix(),
                                     sTransformPipeline.GetProjectionMatrix(),
                                     lightPosition,
                                     sBodyColors[i] );
      sBodyBatch[i].Draw();
      // Restore
      sModelViewMatrixStack.PopMatrix();
   }
   */
}

///////////////////////////////////////////////////////////////////////
// Physics
void updatePhysics( float deltaT ) {
clock_t start_time = clock ();
printf("Update physics ...\n");
cout << "\nBody   :      x          y          z    |     vx         vy         vz\n";
   for(Particle part : Particles)
   {
      printf("Before Update Acc\n");
     // part.print();
      updateAcceleration(&part);

      printf("Before Update Velo\n");
      //part.print();
      updateVelocity( &part, deltaT );

      printf("Before Update Pos\n");
      //part.print();
      updatePosition( &part, deltaT );

      printf("End current update\n");
      part.print();
   }
   printf("End Update physics Take %f\n calc time: %f sec\n", float( clock () - start_time ) );



   /*
   for( int i = 0; i < BODY_COUNT; i++ ) {
      //updateAcceleration( i );
     // updateVelocity( i, deltaT );
     // updatePosition( i, deltaT );
   }*/
}

void updateAcceleration( Particle * _part ) {
   Force3D netForce = { 0, 0, 0 };

  
   for( Particle part : Particles  ) {
      if( part.id == _part->id) {
         continue;
      }

      Force3D vectorForceToOther = {0, 0, 0};
      Force scalarForceBetween = forceNewtonianGravity3D(
                                    _part->mass,
                                    part.mass,
                                    _part->position,
                                    part.position );
      direction( _part->position,
                 part.position,
                 vectorForceToOther );
      vectorForceToOther.x *= scalarForceBetween;
      vectorForceToOther.y *= scalarForceBetween;
      vectorForceToOther.z *= scalarForceBetween;
      netForce.x += vectorForceToOther.x;
      netForce.y += vectorForceToOther.y;
      netForce.z += vectorForceToOther.z;
   }

   _part->acceleration = computeAccel3D( _part->mass,
                                  netForce );
}
 /*
void updateAcceleration( int bodyIndex ) {
   Force3D netForce = { 0, 0, 0 };

   #pragma omp parallel for
   for( int i = 0; i < BODY_COUNT; i++ ) {
      if( i == bodyIndex ) {
         continue;
      }

      Force3D vectorForceToOther = {0, 0, 0};
      Force scalarForceBetween = forceNewtonianGravity3D(
                                    sBodyMass[bodyIndex],
                                    sBodyMass[i],
                                    sBodyPosition[bodyIndex],
                                    sBodyPosition[i] );
      direction( sBodyPosition[bodyIndex],
                 sBodyPosition[i],
                 vectorForceToOther );
      vectorForceToOther.x *= scalarForceBetween;
      vectorForceToOther.y *= scalarForceBetween;
      vectorForceToOther.z *= scalarForceBetween;
      netForce.x += vectorForceToOther.x;
      netForce.y += vectorForceToOther.y;
      netForce.z += vectorForceToOther.z;
   }

   sBodyAcceleration[bodyIndex] = computeAccel3D( sBodyMass[bodyIndex],
                                  netForce );
}

*/

void updateVelocity( Particle * part, float deltaT ) {
   part->velocity = computeVelo3D(
                                 part->acceleration,
                                part->velocity,
                                 deltaT );
}
/*
void updateVelocity( int bodyIndex, float deltaT ) {
   sBodyVelocity[bodyIndex] = computeVelo3D(
                                 sBodyAcceleration[bodyIndex],
                                 sBodyVelocity[bodyIndex],
                                 deltaT );
}
*/

void updatePosition( Particle * part, float deltaT ) {
   part->position = computePos3D( part->velocity,
                              part->position,
                              deltaT );
}
/*
void updatePosition( int bodyIndex, float deltaT ) {
   sBodyPosition[bodyIndex] = computePos3D( sBodyVelocity[bodyIndex],
                              sBodyPosition[bodyIndex],
                              deltaT );
}*/

string readFile(string fileName)
{
   printf("In readFile\n");
    stringstream str;
    cout << "FileName : "<<fileName <<"\n";
    ifstream stream("NKoerper.txt");
    if(stream.is_open())
    {
       printf("opened readFile\n");
        while(stream.peek() != EOF)
        {
           printf("reading readFile\n");
            str << (char) stream.get();
        }
        stream.close();
        printf("Terminated readFile\n");
        return str.str();
    }
    return NULL;
}

