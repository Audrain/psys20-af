#ifndef CONSTANTS_H_GUARD
#define CONSTANTS_H_GUARD


// Configuration
const static bool DEBUG = false;
const static bool TEST = true;
const static int BODY_COUNT = 10;

// GUI constants
const static int APP_WIDTH = 200;
const static int APP_HEIGHT = 100;

// GL constant
const static float APP_CAMERA_FOV = 40.0f;
const static float APP_CAMERA_NEAR = 1.0f;
const static float APP_CAMERA_FAR = 1000.f;

#endif
