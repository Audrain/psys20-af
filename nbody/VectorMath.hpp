#ifndef VECTORMATH_H_
#define VECTORMATH_H_

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdlib.h> 
#include <vector>


// Physics and math constants
constexpr double PI = 3.141592653589f;
constexpr double G = 6.67e-11f;
//const static double Mass = 10.0f;
constexpr double TIMESTAMP = 0.02f;
constexpr int numberofiterations = 2;

struct Particle{
    double rx, ry, rz;//position components
    double vx, vy, vz;//velocity components
    double fx, fy, fz;//force components
    double Mass;

};

std::vector<float> PositionX;
std::vector<float> PositionY;
std::vector<float> PositionZ;

std::vector<float> rotationX;
std::vector<float> rotationY;
std::vector<float> rotationZ;

std::vector<Particle> bodies;


double DistParts(Particle a, Particle b);
static void PrintParticle(Particle p); 
void CalculateAcc(Particle * part, std::vector<Particle> * bodies)
{
    
    part->fx = 0;
     part->fy = 0;
      part->fz = 0;
      double EPS = 0.5f;      // softening parameter (just to avoid infinities)
  // int partCount =  bodies->size();
   for(Particle particule2 : *bodies)
   {
       //Particle particule2 = bodies->at(i);
        double dist = DistParts(*part,particule2);
       part->fx += (particule2.Mass*dist)/sqrt(pow((dist*dist + EPS*EPS), 3));
       
   }
    part->fx *= G;
    part->fy = part->fx;
    part->fz = part->fy;
}

double DistParts(Particle a, Particle b)
{
        double deltaX = b.rx - a.rx;
    double deltaY = b.ry - a.ry;
    double deltaZ = b.rz - a.rz;
    return sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);
}


#endif