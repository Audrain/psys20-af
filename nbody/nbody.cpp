

#include <iostream>
#include <ctime>
#include <math.h>

#include "nbody.hpp"
#include "VectorMath.hpp"

using std::cout;
using std::cerr;
using std::endl;

Particle updatePhysics(Particle p, double timestamp)
{
    p.vx += timestamp*p.fx / p.Mass;
    p.vy += timestamp*p.fy / p.Mass;
    p.vz += timestamp*p.fz / p.Mass;
    p.rx += timestamp*p.vx;
    p.ry += timestamp*p.vy;
    p.rz += timestamp*p.vz;
    return p;
}


void PrintParticle(Particle p) 
{
    printf("rx == %f \nry == %f  \nrz == %f \nvx == %f \nvy == %f \nvz == %f \nmass == %f\n\n", p.rx,p.ry, p.rz,p.vx,p.vy, p.vz,p.Mass);
}


int main(int argc, char **argv)
{
    setupWindow( argc, argv );
    registerCallbacks();
    // Initialize GLEW
    GLenum anError = glewInit();

    if( anError != 0 ) {
        fprintf( stderr, "GLEW Error: %s\n",
               glewGetErrorString( anError ) );

        if( D ) {
         cerr << " done" << endl;
      }

      return 1;
   }

   setupRenderContext();
   glutMainLoop();
   return 0;

    
            //printf("Hallo 1");
    srand(time(NULL));
    //randomly generating N Particles
    for(int i = 0; i<BODY_COUNT; i++)
    {
        Particle part;
        
        part.rx = 1e18*exp(-1.8)*(0.5 - rand());
        part.ry = 1e18*exp(-1.8)*(.5 - rand());;
        part.rz = 1e18*exp(-1.8)*(.5 - rand());;
        part.vx = 1e18*exp(-1.8)*(.5 - rand());
        part.vy = 1e18*exp(-1.8)*(.5 - rand());
        part.vz = 1e18*exp(-1.8)*(.5 - rand());
        part.Mass = 1.98892e30*rand()*10 + 1e20;;
        bodies.push_back(part);
    }


    
    for(int count = 0; count < numberofiterations; ++count){
        printf("*********** Iteration %d *********\n\n", count+1);
            for(Particle part : bodies)
    {

            PrintParticle(part);
            CalculateAcc(&part, &bodies);
            PrintParticle(part);

        }
        //loop again to update the time stamp here
        for(Particle part : bodies)
    {
            part = updatePhysics(part, TIMESTAMP);
        }
        
          for(Particle part : bodies)
    {
            PrintParticle(part);
        }
    }

}



