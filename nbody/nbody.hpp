#ifndef NBODY_H_
#define NBODY_H_

#include <iostream>
#include <string>

#include <GL/glew.h>
#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

// Project libs
#include <GLTools.h>
#include <GLFrustum.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <GLShaderManager.h>
#include <StopWatch.h>
#include <math3d.h>

#include "VectorMath.hpp"


GLuint programID;
GLuint numIndices;

const static bool D = false;
int timerInterval = 2;
const static int BODY_COUNT = 2;
// GUI constants
const static int APP_WIDTH = 400;
const static int APP_HEIGHT = 200;

// GL constant
const static float APP_CAMERA_FOV = 40.0f;
const static float APP_CAMERA_NEAR = 1.0f;
const static float APP_CAMERA_FAR = 1000.f;


// - Matrices and shaders
static GLShaderManager sShaderManager;
static GLFrustum sViewFrustrum;
static GLMatrixStack sProjectionMatrixStack;
static GLMatrixStack sModelViewMatrixStack;
static GLGeometryTransform sTransformPipeline;

static GLFrame sCameraFrame;

const static GLclampf sBackgroundColor[] = {0.0f, 0.0f, 0.0f, 1.0f};
const static M3DVector4f sMainLightPos = {0.0f, 10.0f, 5.0f, 1.0f};

static GLTriangleBatch sBodyBatch[BODY_COUNT];
static GLFrame sBodyFrames[BODY_COUNT];
const static GLfloat sBodyRadius[BODY_COUNT] = {
    20.0f,
    10.0f,} ;
const static GLclampf sBodyColors [BODY_COUNT][4] = {
  {0.8f, 0.8f, 0.1f, 1.0f},
  {0.6f, 0.7f, 0.2f, 1.0f},
}; // Yellow


static void onChangeSize(int aNewWidth, int aNewHeight);
static void onRenderScene();

void drawBodies( M3DVector4f *lightPosition );

// Setup
void setupWindow(int argc, char **argv);
void registerCallbacks();
void setupRenderContext();
void setupBodies();

void setupWindow( int argc, char **argv ) {
   gltSetWorkingDirectory( argv[0] );
   glutInit( &argc, argv );
   glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
   glutInitWindowSize( APP_WIDTH, APP_HEIGHT );
   glutCreateWindow( "Body SImulation" );
  // glutMouseFunc( onMouseEvent );
   //glutIgnoreKeyRepeat(true);
   glutFullScreen();
}

void registerCallbacks() {
   glutReshapeFunc( onChangeSize );
   glutDisplayFunc( onRenderScene );
}

void setupRenderContext() {
   printf ("render print \n");
   sShaderManager.InitializeStockShaders();
   glEnable( GL_DEPTH_TEST );
   setupBodies();
   glClearColor( sBackgroundColor[0],
                 sBackgroundColor[1],
                 sBackgroundColor[2],
                 sBackgroundColor[3] );
   glEnable( GL_LINE_SMOOTH );
}

void setupBodies() {
   int i = 0;
   for( Particle part : bodies) {
   
      gltMakeSphere( sBodyBatch[i], sBodyRadius[i], 30, 50 );
      sBodyFrames[i].SetOrigin( part.rx,
                                part.ry,
                                part.rz );
                                i++;
   }
   printf ("setupbodies23 print \n");
}

///////////////////////////////////////////////////////////////////////
// Callbacks

void onChangeSize( int aNewWidth, int aNewHeight ) {
   glViewport( 0, 0, aNewWidth, aNewHeight );
   sViewFrustrum.SetPerspective( APP_CAMERA_FOV,
                                 float( aNewWidth ) / float( aNewHeight ),
                                 APP_CAMERA_NEAR,
                                 APP_CAMERA_FAR );
   sProjectionMatrixStack.LoadMatrix(
      sViewFrustrum.GetProjectionMatrix() );
   sTransformPipeline.SetMatrixStacks( sModelViewMatrixStack,
                                       sProjectionMatrixStack );
}

void onRenderScene( void ) {
   // Clear the buffer
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   static CStopWatch timeKeeper;
   // Place camera
   M3DMatrix44f mCamera;
   sCameraFrame.GetCameraMatrix( mCamera );
   sModelViewMatrixStack.PushMatrix( mCamera );
   // Transform the light position into eye coordinates
   M3DVector4f lightPos = { sMainLightPos[0],
                            sMainLightPos[1],
                            sMainLightPos[2],
                            sMainLightPos[3]
                          };
   M3DVector4f lightEyePos;
   m3dTransformVector4( lightEyePos, lightPos, mCamera );
   // Call the drawing functions
   // Switch the buffers to bring the drawing on screen
   glutSwapBuffers();
   glutPostRedisplay();
}

void drawBodies( M3DVector4f *lightPosition ) {
   // compute displacement and new vectors
   
   int i = 0;
   for( Particle part : bodies) {
       
       //updatePhysics(part, currentTime-previousTime);
       // Save
      sModelViewMatrixStack.PushMatrix();
      // update position with regard to new values
      sBodyFrames[i].SetOrigin( part.rx,
                                part.ry,
                                part.rz );
    // draw
      sModelViewMatrixStack.MultMatrix( sBodyFrames[i] );
      sShaderManager.UseStockShader( GLT_SHADER_POINT_LIGHT_DIFF,
                                     sTransformPipeline.GetModelViewMatrix(),
                                     sTransformPipeline.GetProjectionMatrix(),
                                     lightPosition,
                                     sBodyColors[i] );
      sBodyBatch[i].Draw();
      // Restore
      sModelViewMatrixStack.PopMatrix();
      i++;
   }

}



#endif