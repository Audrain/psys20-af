#ifndef VECTORMATH_H_
#define VECTORMATH_H_

#include <iostream>
#include <math.h>

const static double PI = 3.141592653589f;
const static double G = 6.67e-11f; // Gravitational constant

// Vectors declarations
typedef double Vector;
typedef Vector Force;
typedef Vector Acceleration;     // acceleration = velocity over the time = deltaV/deltaT
typedef Vector Position;         // postion of the time
typedef Vector Velocity;         // velocity = position over the time = dela^2R/ deltaT

// Structur of our 3D Bodies

struct Vector3D
{
    Vector x;
    Vector y;
    Vector z;
};

typedef Vector3D Force3D;
typedef Vector3D Acceleration3D;
typedef Vector3D Velocity3D;
typedef Vector3D Position3D;

typedef double Scalar;
typedef Scalar Mass;
typedef Scalar Time;

// calculate the Length of a Vectore

inline Scalar magnitude( Vector3D &v){
    Scalar squareOfV = 0.0;
    squareOfV += v.x * v.x;
    squareOfV += v.y * v.y;
    squareOfV += v.z * v.z;

    return sqrt (squareOfV);
}

inline void invert(Vector3D &v){
    v.x *= -1.0;
    v.y *= -1.0;
    v.z *= -1.0;
}

inline void normalize( Vector3D &v){
    Scalar length = magnitude(v);
    v.x = v.x / length;
    v.y = v.y / length;
    v.z = v.z / length;
}

inline void direction( const Vector3D &vectorA, const Vector3D &vectorB, Vector3D &vecDirection) {
    vecDirection.x = vectorB.x - vectorA.x;
    vecDirection.y = vectorB.y - vectorA.y;
    vecDirection.z = vectorB.z - vectorA.z;
    normalize(vecDirection);
}

// Force Newton Universal Gravitation, Compute Force between two Bodies
inline Force addForce3D( Mass aMass , Mass bMass,
                        Position3D aPosition, Position3D bPosition ) 
{
    double EPS = 0.5f; //softening parameter (to avoid infinities)
    Scalar deltaX = bPosition.x - aPosition.x;
    Scalar deltaY = bPosition.y - aPosition.y;
    Scalar deltaZ = bPosition.z - aPosition.z;
    Scalar distance = sqrt ( deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ ); //calculate the distance between Bodies

    Force F = (G * aMass * bMass) / (distance * distance + EPS * EPS);

    return F;

}

//Newton law F = m * a => a = F / m
inline Acceleration computeAcceleration(Mass mass, Force force){
    if (force == 0 ){
        return 0;
    }
    Scalar acc = force / mass;
    return acc;
}

// acceleration is the change of velocity over the Time
// acceleration = deltaV / delaT
inline Velocity computeVelocity(Acceleration actuelAcc,
                                Velocity previousVelo,
                                Time deltaT ){
    return previousVelo + (actuelAcc * deltaT);
                    
}

// velocity is the Change op Position over the Time 
//Velocity = deltaR / deltaT
inline Position computePos( Velocity actuelVelo,
                            Position previousPos,
                            Time deltaT ) {
   return previousPos + ( actuelVelo * deltaT );
}

inline Acceleration3D computeAccel3D( Mass mass, const Force3D &force ) {
   Acceleration3D acc3D = {0, 0, 0};
   acc3D.x = computeAcceleration( mass, force.x );
   acc3D.y = computeAcceleration( mass, force.y );
   acc3D.z = computeAcceleration( mass, force.z );
   return acc3D;
}

inline Velocity3D computeVelo3D( Acceleration3D &accel,
                                 Velocity3D &prevVelo,
                                 Time deltaT ) {
   Velocity3D aVelocity = {0, 0, 0};
   aVelocity.x = computeVelocity( accel.x, prevVelo.x, deltaT );
   aVelocity.y = computeVelocity( accel.y, prevVelo.y, deltaT );
   aVelocity.z = computeVelocity( accel.z, prevVelo.z, deltaT );
   return aVelocity;
}

inline Position3D computePos3D( Velocity3D &velo,
                                Position3D &prevPos,
                                Time deltaT ) {
   Position3D PositionVector = {0, 0, 0};
   PositionVector.x = computePos( velo.x, prevPos.x, deltaT );
   PositionVector.y = computePos( velo.y, prevPos.y,  deltaT );
   PositionVector.z = computePos( velo.z, prevPos.z,  deltaT );
   return PositionVector;
}

#endif