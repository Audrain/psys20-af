#include "Nkoerper.h"
#include "VectorMath.h"

using std::cout;
using std::cerr;
using std::endl;
///////////////////////////////////////////////////////////////////////
// Setup

int main( int argc, char **argv ) {
  // printGreetings();
   setupWindow( argc, argv );
   registerCallbacks();

   // Initialize GLEW
   GLenum anError = glewInit();

   if( anError != 0 ) {
      fprintf( stderr, "GLEW Error: %s\n",
               glewGetErrorString( anError ) );

      if( D ) {
         cerr << " done" << endl;
      }

      return 1;
   }

   setupRenderContext();
   glutMainLoop();
   return 0;
}

void setupWindow( int argc, char **argv ) {
   gltSetWorkingDirectory( argv[0] );
   glutInit( &argc, argv );
   glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
   glutInitWindowSize( APP_WIDTH, APP_HEIGHT );
   glutCreateWindow( "Body SImulation" );
   glutMouseFunc( onMouseEvent );
   glutFullScreen();
}

void registerCallbacks() {
   glutReshapeFunc( onChangeSize );
   glutDisplayFunc( onRenderScene );
}

void setupRenderContext() {
   sShaderManager.InitializeStockShaders();
   glEnable( GL_DEPTH_TEST );
   setupBodies();
   glClearColor( sBackgroundColor[0],
                 sBackgroundColor[1],
                 sBackgroundColor[2],
                 sBackgroundColor[3] );
   glEnable( GL_LINE_SMOOTH );
}

void show_Bodies() {
   for( int i = 0; i < BODIES; i++ ) {

      gltMakeSphere( sBodyBatch[i], sBodyRadius[i], 10, 20 );
      sBodyFrames[i].SetOrigin( startPosition[i].x,
                                startPosition[i].y,
                                startPosition[i].z );

	printf("\nBody %d:\nMass: %f\nPosition(x ,y, z): %f, %f, %f\nVelocity(x, y, z): %f, %f, %f\nAcceleration(x ,y, z): %f, %f, %f\n\n",
      i + 1, 
      BodyMass[i], 
      startPosition[i].x, startPosition[i].y, startPosition[i].z,
      startVelocity[i].x, startVelocity[i].y, startVelocity[i].z,
      startAcceleration[i].x, startAcceleration[i].y, startAcceleration[i].z);
   }
}

///////////////////////////////////////////////////////////////////////
// Callbacks

void onChangeSize( int aNewWidth, int aNewHeight ) {
   glViewport( 0, 0, aNewWidth, aNewHeight );
   sViewFrustrum.SetPerspective( APP_CAMERA_FOV,
                                 float( aNewWidth ) / float( aNewHeight ),
                                 APP_CAMERA_NEAR,
                                 APP_CAMERA_FAR );
   sProjectionMatrixStack.LoadMatrix(
      sViewFrustrum.GetProjectionMatrix() );
   sTransformPipeline.SetMatrixStacks( sModelViewMatrixStack,
                                       sProjectionMatrixStack );
}

void onRenderScene( void ) {
   // Clear the buffer
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   static CStopWatch timeKeeper;
   // Place camera
   M3DMatrix44f mCamera;
   sCameraFrame.GetCameraMatrix( mCamera );
   sModelViewMatrixStack.PushMatrix( mCamera );
   // Transform the light position into eye coordinates
   M3DVector4f lightPos = { sMainLightPos[0],
                            sMainLightPos[1],
                            sMainLightPos[2],
                            sMainLightPos[3]
                          };
   M3DVector4f lightEyePos;
   m3dTransformVector4( lightEyePos, lightPos, mCamera );
   // Call the drawing functions
   drawBodies( &timeKeeper, &lightEyePos );
   // Switch the buffers to bring the drawing on screen
   glutSwapBuffers();
   glutPostRedisplay();
}


///////////////////////////////////////////////////////////////////////
// Drawing

void drawBodies( CStopWatch *timeKeeper, M3DVector4f *lightPosition ) {
   // compute displacement and new vectors
   static float previousTime = 0.0f;
   float currentTime = timeKeeper->GetElapsedSeconds();
   updatePhysics( currentTime - previousTime );
   previousTime = currentTime;

   for( int i = 0; i < BODIES; i++ ) {
      // Save
      sModelViewMatrixStack.PushMatrix();
      // update position with regard to new values
      sBodyFrames[i].SetOrigin( startPosition[i].x,
                                startPosition[i].y,
                                startPosition[i].z );
      // draw
      sModelViewMatrixStack.MultMatrix( sBodyFrames[i] );
      sShaderManager.UseStockShader( GLT_SHADER_POINT_LIGHT_DIFF,
                                     sTransformPipeline.GetModelViewMatrix(),
                                     sTransformPipeline.GetProjectionMatrix(),
                                     lightPosition,
                                     BodyColors[i] );
      sBodyBatch[i].Draw();
      // Restore
      sModelViewMatrixStack.PopMatrix();
   }
}

///////////////////////////////////////////////////////////////////////
// Physics

void updatePhysics( float deltaT ) {
   for( int i = 0; i < BODIES; i++ ) {
      updateAcceleration( i );
      updateVelocity( i, deltaT );
      updatePosition( i, deltaT );
   }
}

void updateAcceleration( int bodyIndex ) {
   Force3D netForce = { 0, 0, 0 };

   for( int i = 0; i < BODIES; i++ ) {
      if( i == bodyIndex ) {
         continue;
      }

      Force3D vectorForceToOther = {0, 0, 0};
      Force scalarForceBetween = addForce3D(
                                    BodyMass[bodyIndex],
                                    BodyMass[i],
                                    startPosition[bodyIndex],
                                    startPosition[i] );
      direction( startPosition[bodyIndex],
                 startPosition[i],
                 vectorForceToOther );
      vectorForceToOther.x *= scalarForceBetween;
      vectorForceToOther.y *= scalarForceBetween;
      vectorForceToOther.z *= scalarForceBetween;
      netForce.x += vectorForceToOther.x;
      netForce.y += vectorForceToOther.y;
      netForce.z += vectorForceToOther.z;
   }

   startAcceleration[bodyIndex] = computeAccel3D( BodyMass[bodyIndex],
                                  netForce );
}

void updateVelocity( int bodyIndex, float deltaT ) {
   startVelocity[bodyIndex] = computeVelo3D(
                                 startAcceleration[bodyIndex],
                                 startVelocity[bodyIndex],
                                 deltaT );
}

void updatePosition( int bodyIndex, float deltaT ) {
   startPosition[bodyIndex] = computePos3D( startVelocity[bodyIndex],
                              startPosition[bodyIndex],
                              deltaT );
}

