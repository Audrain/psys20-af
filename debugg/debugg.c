#include <stdio.h>
int main(int argc, char **argv) 
{
    int i; 
    double x, 
    sum = 0.0;
    double step; 
    int num_steps = 100000;

    step = 1.0 / (double)num_steps;

#pragma omp parallel for 
for (i=0; i < num_steps; i++) 
{ 
    x = (i+0.5)*step; 
    sum = sum + 1.0/(1.0+x*x); 
} 
    sum *= step; 
    printf("sum=%lf\n",sum); return 0;
}