/**
 * Set OpenGL Shaders and Nkoerper Methods
 */

#ifndef NBODY_H_
#define NBODY_H_

#include <iostream>
#include <string>
#include <GL/glew.h>

#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#endif

// Project libs
#include <GLTools.h>
#include <GLFrustum.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <GLShaderManager.h>
#include <StopWatch.h>
#include <math3d.h>
// Local
#include "Constant.h"
#include "vectorMath.h"

const static bool D = DEBUG;

static GLShaderManager sShaderManager;
static GLFrustum sViewFrustrum;
static GLMatrixStack sProjectionMatrixStack;
static GLMatrixStack sModelViewMatrixStack;
static GLGeometryTransform sTransformPipeline;

// Create Bodies and GUI

static GLFrame sCameraFrame;

const static GLclampf sBackgroundColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
const static M3DVector4f sMainLightPos = { 0.0f, 10.0f, 5.0f, 1.0f };

static GLTriangleBatch sBodyBatch[BODIES];
static GLFrame sBodyFrames[BODIES];
const static GLfloat sBodyRadius[BODIES] = {
   10.0f,
   15.0f,
   20.0f,
};

// give a color to the bodies
const static GLclampf BodyColors[BODIES][4] = {
   {0.8f, 0.8f, 0.1f, 1.0f},  // Yellow
   //{0.5f, 0.5f, 1.0f, 1.0f},  // Blue
   //{0.8f, 0.8f, 1.0f, 1.0f},  // Light-blue
   //{0.8f, 1.0f, 0.8f, 1.0f},  // Light-green
   //{1.0f, 1.0f, 1.0f, 1.0f},  // White
   //{0.9f, 0.1f, 0.2f, 1.0f}   // Dark red
};

static Position3D startPosition[BODIES] = {
   //{ 0.0f, 0.0f, -1000.0f },
   { 0.0f, 200.0f, -1000.0f },
   { -200.0f, 0.0f, -1000.0f },
  // { 0.0f, 0.0f, -800.0f },
   { -100.0f, 0.0f, -2000.0f },
  // { 100.0f, 0.0f, -500.0f }
};
static Velocity3D startVelocity[BODIES] = {
   //{ 0.0f, 0.0f, 0.0f },
   { -30.0f, -30.0f, -30.0f },
   { 30.0f, 30.0f, 30.0f },
  // { 45.0f, -30.0f, 15.0f },
   { -30.0f, 20.0f, -45.0f},
   //{ -30.0f, 20.0f, 10.0f}
};
static Acceleration3D startAcceleration[BODIES] = {
   //{ 0.0f, 0.0f, 0.0f },
   { 0.0f, 0.0f, 0.0f },
   { 0.0f, 0.0f, 0.0f },
   //{ 0.0f, 0.0f, 0.0f },
   { 0.0f, 0.0f, 0.0f },
   //{ 0.0f, 0.0f, 0.0f }
};
static GLfloat BodyMass[BODIES] = {
   //1e16f,
   1e1f,
   1e1f,
   //2e1f,
   3e1f,
   //2e1f
};

//setup Windows and render bodies and Scene
void setupWindow( int argc, char **argv );
void registerCallbacks();
void setupRenderContext();
void setupBodies();

// Callbacks
static void onChangeSize( int aNewWidth, int aNewHeight );
static void onRenderScene();
static void onMouseEvent( int key, int x, int y, int something );

// Drawing
static void drawBodies( CStopWatch *timeKeeper,
                        M3DVector4f *lightPosition );

static void updateBodies( float deltaT );
static void updateAcceleration( int bodyIndex );
static void updateVelocity( int bodyIndex, float deltaT );
static void updatePosition( int bodyIndex, float deltaT );

#endif

