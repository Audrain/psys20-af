/**
 *  Physics und OpenGL constant für die Nkoerper Simulation
 *  Audrain Fanko; Amanda 
 **/


#ifndef CONSTANTS_H_GUARD
#define CONSTANTS_H_GUARD


// Configuration
const static bool DEBUG = false;
const static bool TEST = true;
const static int BODIES = 3;        // We start hier with N = 3

// GL GUI constants
const static int APP_WIDTH = 600;
const static int APP_HEIGHT = 400;

// GL constant
const static float APP_CAMERA_FOV = 40.0f;
const static float APP_CAMERA_NEAR = 1.0f;
const static float APP_CAMERA_FAR = 10000.f;

#endif
 