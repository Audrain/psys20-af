/* 
 c program:
 --------------------------------
  1. draws Mandelbrot set for Fc(z)=z*z +c
  using Mandelbrot algorithm ( boolean escape time )
 -------------------------------         
 2. technique of creating ppm file is  based on the code of Claudio Rocchini
 http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
 create 24 bit color graphic file ,  portable pixmap file = PPM 
 see http://en.wikipedia.org/wiki/Portable_pixmap
 to see the file use external application ( graphic viewer)
  */
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <math.h>

 int main(int argc, char **argv)
 {
          /* screen ( integer) coordinate */
        int iX,iY;
        int iXmax = 800; 
        int iYmax = 800;
        int xOffset = 0;
        int yOffset = 0;
        if(argc > 1){
            iXmax = atoi(argv[1]);
            iYmax = atoi(argv[2]);
            xOffset = atoi(argv[3]);
            yOffset = atoi(argv[4]);
        }
        /* world ( double) coordinate = parameter plane*/
        double Cx,Cy;
        const double CxMin=-2.5;
        const double CxMax=1.5;
        const double CyMin=-2.0;
        const double CyMax=2.0;
        /* */
        double PixelWidth=(CxMax-CxMin)/iXmax;
        double PixelHeight=(CyMax-CyMin)/iYmax;
        /*  */
        const int IterationMax=200;
        /* bail-out value , radius of circle ;  */
        const double EscapeRadius=2;
        double ER2=EscapeRadius*EscapeRadius;

        /* compute and write image data bytes to the file*/
        unsigned char img[iYmax * iXmax][3];
        
        /* compute and write image data bytes to the file*/
        #pragma omp parallel for firstprivate(iYmax, iXmax, CyMin, PixelHeight, ER2, IterationMax, xOffset, yOffset) shared(img)
        for(iY=0;iY<iYmax;iY++)
        {
             Cy=CyMin + (iY+yOffset)*PixelHeight;
             if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
             for(iX=0;iX<iXmax;iX++)
             {         
                        Cx=CxMin + (iX+ xOffset)*PixelWidth;
                        /* initial value of orbit = critical point Z= 0 */
                        double Zx=0.0;
                        double Zy=0.0;
                        double Zx2=Zx*Zx;
                        double Zy2=Zy*Zy;
                        /* */
                        int Iteration;
                        for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++)
                        {
                            Zy=2*Zx*Zy + Cy;
                            Zx=Zx2-Zy2 +Cx;
                            Zx2=Zx*Zx;
                            Zy2=Zy*Zy;
                        };

                        /* compute  pixel color (24 bit = 3 bytes) */

                        if (Iteration!=IterationMax)
                        { /*  interior of Mandelbrot set = black */
                            img[iY * iXmax + iX][0]=255;
                            img[iY * iXmax + iX][1]=255;
                            img[iY * iXmax + iX][2]=255;
                          
                        }
                        
                }
        }

        FILE *fp;
        char *filename="mandelbrotImg.ppm";
        char *comment="# ";
        fp= fopen(filename,"wb");
        fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax, 255);
        int y;
        int x;
        for (y = 0; y < iYmax; y++)
        {
            for (x = 0; x < iXmax; x++)
            {
                fwrite(&img[y * iXmax + x],1,3,fp);
            }
        }

        fclose(fp);
        return 0;
 }