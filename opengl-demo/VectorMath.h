#ifndef VECTORMATH_H_
#define VECTORMATH_H_

#include <iostream>
#include <math.h>

const static double PI = 3.141592653589f;
const static double G = 6.67e-11f; // Gravitational constant

// Vectors declarations
typedef double Vector;
typedef Vector Force;
typedef Vector Acceleration;     // acceleration = velocity over the time = deltaV/deltaT
typedef Vector Position;         // postion of the time
typedef Vector Velocity;         // velocity = position over the time = delta^2R/ deltaT

// 3D Vector structur

typedef struct Vector3D {
   Vector x;
   Vector y;
   Vector z;
};
typedef Vector3D Force3D;
typedef Vector3D Acceleration3D;
typedef Vector3D Velocity3D;
typedef Vector3D Position3D;

// Scalar
typedef double Scalar;
typedef Scalar Mass;
typedef Scalar Time;

//Vector Math
// Length of a vectore = Magnitude
inline Scalar magnitude( const Vector3D &aVector ) {
   Scalar squareOfA = 0.0;
   squareOfA += aVector.x * aVector.x;
   squareOfA += aVector.y * aVector.y;
   squareOfA += aVector.z * aVector.z;
   return sqrt( squareOfA );
}

// normalize component of a vector
inline void normalize( Vector3D &aVector ) {
   Scalar length = magnitude( aVector );
   aVector.x = aVector.x / length;
   aVector.y = aVector.y / length;
   aVector.z = aVector.z / length;
}

inline void invert( Vector3D &aVector ) {
   aVector.x *= -1.0;
   aVector.y *= -1.0;
   aVector.z *= -1.0;
}

inline void direction( const Vector3D &fromVector,
                       const Vector3D &toVector,
                       Vector3D &resultVector ) {
   resultVector.x = toVector.x - fromVector.x;
   resultVector.y = toVector.y - fromVector.y;
   resultVector.z = toVector.z - fromVector.z;
   normalize( resultVector );
}

// Physics operations

// Force Newton Universal Gravitation, Compute Force between two Bodies
inline Force forceNewtonianGravity3D( Mass onMass, Mass becauseOfMass,
                                      Position3D onPosition, Position3D becauseOfPosition ) 
{
   double EPS = 0.01f; //softening parameter (to avoid infinities) 
   Scalar deltaX = becauseOfPosition.x - onPosition.x;
   Scalar deltaY = becauseOfPosition.y - onPosition.y;
   Scalar deltaZ = becauseOfPosition.z - onPosition.z;
   Scalar distance = sqrt ( deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ ); //calculate the distance between Bodies

   if( distance == 0 ) {
      return 0;
   }

   Force F = G * ( onMass * becauseOfMass ) / ( distance * distance + EPS * EPS);  // onMass ist the mass of Bodie a and because of Mass is for Bodie b
   return F;
}

//Newton law F = m * a => a = F / m  
inline Acceleration computeAccel( Mass mass, Force force ) {
   if( force == 0 ) {
      return 0;
   }

   Scalar acc = force / mass;
   return acc;
}

// acceleration = deltaV / deltaT
inline Velocity computeVelo( Acceleration current,
                             Velocity previous,
                             Time deltaT ) {
   return previous + ( current * deltaT );
}


inline Position computePos( Velocity current,
                            Position previous,
                            Time deltaT ) {
   return previous + ( current * deltaT );
}

inline Acceleration3D computeAccel3D( Mass mass, const Force3D &force ) {
   Acceleration3D acc3D = {0, 0, 0};
   acc3D.x = computeAccel( mass, force.x );
   acc3D.y = computeAccel( mass, force.y );
   acc3D.z = computeAccel( mass, force.z );
   return acc3D;
}

inline Velocity3D computeVelo3D( Acceleration3D &accel,
                                 Velocity3D &prevVelo,
                                 Time deltaT ) {
   Velocity3D aVelocity = {0, 0, 0};
   aVelocity.x = computeVelo( accel.x, prevVelo.x, deltaT );
   aVelocity.y = computeVelo( accel.y, prevVelo.y, deltaT );
   aVelocity.z = computeVelo( accel.z, prevVelo.z, deltaT );
   return aVelocity;
}

inline Position3D computePos3D( Velocity3D &velo,
                                Position3D &prevPos,
                                Time deltaT ) {
   Position3D anPositionVector = {0, 0, 0};
   anPositionVector.x = computePos( velo.x, prevPos.x, deltaT );
   anPositionVector.y = computePos( velo.y, prevPos.y,  deltaT );
   anPositionVector.z = computePos( velo.z, prevPos.z,  deltaT );
   return anPositionVector;
}

bool testVectorMath( void ) {
   double tolerateError = 0.01;
   Mass firstMass = 6.0;
   Mass secondMass = 4.0;
   Position3D firstPos = {0, 0, 0};
   Position3D secondPos = {0.02, 0, 0};
   double expected = 4.00e-6;
   double result = forceNewtonianGravity3D( firstMass, secondMass,
                   firstPos, secondPos );

   if( fabs( expected - result ) > tolerateError ) {
      return false;
   }

   return true;
}

#endif