#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/norm.hpp>

const auto fragmentShaderSources = R""(
#version 330 core
out vec4 FragColor;
void main() {
    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
}
)"";

const auto vertexShaderSources = R""(
#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in float mass;

uniform mat4 mvp;

void main() {
    gl_Position = mvp * vec4(pos, 1.0f);
    //gl_Position = vec4(pos / 100000, 1.0);
    //gl_PointSize = max(1.0f, mass / 200000000);
    gl_PointSize = 4;;
}
)"";
