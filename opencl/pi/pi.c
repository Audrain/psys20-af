/**
 * @file pi.c
 *
 * This is the main driver of the program, i.e.,
 * the program, which is then used by the user.
 */
#include <stdio.h>
#include <stdlib.h>

#include "picore.h"

int main(int argc, char **argv)
{
	float pi_result;
	int num_of_steps;

	if (argc > 1)
	{
		num_of_steps = atoi(argv[1]);
	} else
	{
		num_of_steps = 1000000;
	}

	pi_result = pi(num_of_steps);

	printf("%f\n",pi_result);
	return EXIT_SUCCESS;
}
