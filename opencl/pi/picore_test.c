/**
 * Test for functions in integrate.c
 */

#include <assert.h>
#include <math.h>

/* Include the module under testing */
#include "picore.c"

/******************************************************/

/* Define an own macro for asserting double values */
#define assertEqualsDouble(expected,actual,epsilon) \
	assert(fabs(expected-actual)<epsilon)

/******************************************************/

static void test__pi_works(void)
{
	assertEqualsDouble(3.14159,pi(1000000),0.00001);
}

/******************************************************/


/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
	test__pi_works();
	return 0;
}
