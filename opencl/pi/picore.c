/**
 * Implements the integration.
 *
 * @file integrate.c
 */

#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "picore.h"

/**
 * Approximates pi.
 *
 * @param steps the number of steps used for the numerical integration.
 *  The more steps the better is the approximation.
 * @return the approximated pi.
 */

float pi(int steps)
{
	int i;
	double l = 0;
	double r = 1;
	double step_size = (r - l) / (double)steps;
	double sum = 0.0;

	for (i=0; i < steps; i++)
	{
		double x = (i+0.5)*step_size;
		sum +=  step_size * (1/(1+x*x));
	}
	return sum*4;
}
